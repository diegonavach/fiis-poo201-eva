create table jugador(
                       id serial primary key ,
                       nombre varchar(128) not null,
                       DNI varchar(12) not null,
                       fecha_nacimiento date,
                       peso int,
                       talla int,
                       fecha_comienzo date not null,
                       club_actual_id int,
                       foreign key (club_actual_id)
);
create table espectador(
                          id serial primary key ,
                          espec_name varchar(64),
);
create table club(
                    id serial primary key ,
                    nombre varchar(32) not null,
                    año_fundacion int,
                    direccion text,
                    lema text,
                    urbanizacion text not null
);
create table partida(
                    id serial primary key ,
                    jugador1_id int not null,
                    jugador2_id int,
                    start_time timestamp,
                    end_time timestamp,
                    fecha date not null,
                    id_sala int not null,
                    id_campeonato int not null,
                    foreign key (id_sala) references sala,
                    foreign key (id_campeonato) references campeonato,
                    foreign key (jugador1) references jugador,
                    foreign key (jugador2) references jugador
);
create table campeonato(
                    id serial primary key ,
                    nombre text not null,
                    campeon_id int,
                    pais text,
                    estado text,
                    fecha_inicio date not null,
                    ciudad text not null,
                    foreign key (campeon_id) references jugador
);

create table sala(
                    id serial primary key ,
                    disponible boolean,
                    partida_actual_id int,
                    max_espectadores int,
                    foreign key (partida_actual_id) references partida

);
insert into jugador values ('Juan Perez Valderrama Delgado','12345678','1990-06-11',90,30,'2020-06-11',null);
insert into jugador values ('Juan Luis Quispe Flores','62545678','1991-07-21',105,37,'2020-06-21',null);
insert into club values ('Univesitario',1970,null,null,'Rimac');
insert into club values ('Alianza del Rimac',1973,null,null,'Rimac');
insert into sala values (0,null,40);
insert into sala values (0,null,50);
insert into campeonato values ('Lo mejor',null,null,'Lima','2020-07-04','La Victoria');
insert into campeonato values ('Lo mejor de lo mejor',null,null,'Pasco','2020-08-04','Cerro de Pasco');
insert into partida values (1,null,null,'2020-08-03',1,1);
insert into partida values (1,2,null,'2020-08-04',1,1);
insert into espectador values ('Pepito');
insert into espectador values ('Lucho');
insert into espectador values ('Diego');






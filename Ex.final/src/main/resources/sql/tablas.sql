CREATE TABLE persona(
                        cod serial PRIMARY KEY,
                        edad int not null,
                        nombres varchar(40) not null,
                        apellidos varchar(40) not null,
                        DNI char (8) not null,
                        familia int not null,
                        entidad_bancaria int,
                        ingresos integer,
                        FOREIGN KEY(familia) REFERENCES familia,
                        FOREIGN KEY(entidad_bancaria) REFERENCES entidad_bancaria
);
CREATE TABLE jefe(
                        cod serial PRIMARY KEY,
                        edad int not null,
                        nombres varchar(40) not null,
                        apellidos varchar(40) not null,
                        DNI char (8) not null,
                        familia int not null,
                        entidad_bancaria int not null,
                        ingresos integer,
                        FOREIGN KEY(familia) REFERENCES familia,
                        FOREIGN KEY(entidad_bancaria) REFERENCES entidad_bancaria
);

CREATE TABLE familia(
                        cod serial PRIMARY KEY,
                        direccion text not null,
                        electrodomesticos text
);

CREATE TABLE entidad_bancaria(
                        cod serial PRIMARY KEY,
                        direccion text not null,
                        nombre text not null
);

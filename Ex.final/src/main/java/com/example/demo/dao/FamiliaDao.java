package com.example.demo.dao;

import com.example.demo.model.Familia;

import java.util.List;

public interface FamiliaDao {
    List<Familia> selectAllFamilias();
    List<Familia> añadirFamilia(Familia familia);
}

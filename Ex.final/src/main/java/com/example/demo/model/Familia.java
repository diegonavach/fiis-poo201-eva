package com.example.demo.model;

public class Familia {
    private final Integer cod;
    private final String direccion;
    private String electrodomesticos;

    public Familia(Integer cod, String direccion, String electrodomesticos) {
        this.cod = cod;
        this.direccion = direccion;
        this.electrodomesticos = electrodomesticos;
    }

    public Integer getCod() {
        return cod;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getElectrodomesticos() {
        return electrodomesticos;
    }

    public void setElectrodomesticos(String electrodomesticos) {
        this.electrodomesticos = electrodomesticos;
    }
}
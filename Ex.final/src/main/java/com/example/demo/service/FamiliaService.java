package com.example.demo.service;

import com.example.demo.model.Familia;
import com.example.demo.dao.FamiliaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FamiliaService {

    private final FamiliaDao familiaDao;

    @Autowired
    public FamiliaService(@Qualifier("FamiliaDao") FamiliaDao familiaDao) {
        this.familiaDao = familiaDao;
    }
    public List<Familia> getAllFamilias(){
        return familiaDao.selectAllFamilias();
    }
}
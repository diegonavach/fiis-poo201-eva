package com.example.demo.api;

import com.example.demo.model.Familia;
import com.example.demo.service.FamiliaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("familia")
@RestController
public class FamiliaController {

    private final FamiliaService familiaService;

    @Autowired
    public FamiliaController(FamiliaService familiaService) {
        this.familiaService = familiaService;
    }

    @GetMapping
    public List<Familia> getAllFamilias() {
        return familiaService.getAllFamilias();
    }
}

package com.company;

import java.util.Date;

public class Motocicleta {
    private String placa;
    private String modelo;
    private String marca;
    private String pol_seguro;
    private double vel_prom;
    private Date fecha_asig;
    private Empresa emp;

    public Motocicleta(String placa, String modelo, String marca, String pol_seguro, double vel_prom, Date fecha_asig,Empresa emp) {
        this.placa = placa;
        this.modelo = modelo;
        this.marca = marca;
        this.pol_seguro = pol_seguro;
        this.vel_prom = vel_prom;
        this.fecha_asig = fecha_asig;
        this.emp=emp;
        emp.añadirmoto(this);
    }


    public Empresa getEmp() {
        return emp;
    }

    public void setEmp(Empresa emp) {
        this.emp = emp;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPol_seguro() {
        return pol_seguro;
    }

    public void setPol_seguro(String pol_seguro) {
        this.pol_seguro = pol_seguro;
    }

    public double getVel_prom() {
        return vel_prom;
    }

    public void setVel_prom(double vel_prom) {
        this.vel_prom = vel_prom;
    }

    public Date getFecha_asig() {
        return fecha_asig;
    }

    public void setFecha_asig(Date fecha_asig) {
        this.fecha_asig = fecha_asig;
    }
}

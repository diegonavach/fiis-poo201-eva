package com.company;

import java.util.ArrayList;

public class Empresa {
    private ArrayList<Usuario> usuarios=new ArrayList<>();
    private ArrayList<Motorizado> empleados=new ArrayList<>();
    private ArrayList<Motocicleta> motos=new ArrayList<>();
    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public Empresa(String nombre) {
        this.nombre = nombre;
    }
    public void añadirUsuario(Usuario u){
        usuarios.add(u);
    }
    public void añadirMotorizado(Motorizado u){
        empleados.add(u);
    }
    public void añadirmoto(Motocicleta u){
        motos.add(u);
    }
}

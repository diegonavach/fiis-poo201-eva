package com.company;
import java.util.ArrayList;

public abstract class Persona {
    private String DNI;
    private String nombre;
    private ArrayList<String> direcciones;
    private int telefono;

    public Persona(String DNI, String nombre, ArrayList<String> direcciones, int telefono) {
        this.DNI = DNI;
        this.nombre = nombre;
        this.direcciones = direcciones;
        this.telefono = telefono;
    }
}

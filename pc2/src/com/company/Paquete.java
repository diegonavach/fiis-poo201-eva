package com.company;
import java.util.Date;

public class Paquete{
    private String cod;
    private Datos_producto datos;
    private Usuario destinatario;
    private Cordenada punto_partida;
    private Cordenada punto_recojo;
    private Date fecha_recepcion;
    private Date fecha_entrega;
    private String estado;

    public Paquete(String cod, Datos_producto datos, Usuario destinatario, Cordenada punto_partida, Cordenada punto_recojo, Date fecha_recepcion, Date fecha_entrega, String estado) {
        this.cod = cod;
        this.datos = datos;
        this.destinatario = destinatario;
        this.punto_partida = punto_partida;
        this.punto_recojo = punto_recojo;
        this.fecha_recepcion = fecha_recepcion;
        this.fecha_entrega = fecha_entrega;
        this.estado = estado;
    }



    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Datos_producto getDatos() {
        return datos;
    }

    public void setDatos(Datos_producto datos) {
        this.datos = datos;
    }

    public Usuario getDestinatario() {
        return destinatario;
    }

    public void setDestinatario(Usuario destinatario) {
        this.destinatario = destinatario;
    }

    public Cordenada getPunto_partida() {
        return punto_partida;
    }

    public void setPunto_partida(Cordenada punto_partida) {
        this.punto_partida = punto_partida;
    }

    public Cordenada getPunto_recojo() {
        return punto_recojo;
    }

    public void setPunto_recojo(Cordenada punto_recojo) {
        this.punto_recojo = punto_recojo;
    }

    public Date getFecha_recepcion() {
        return fecha_recepcion;
    }

    public void setFecha_recepcion(Date fecha_recepcion) {
        this.fecha_recepcion = fecha_recepcion;
    }

    public Date getFecha_entrega() {
        return fecha_entrega;
    }

    public void setFecha_entrega(Date fecha_entrega) {
        this.fecha_entrega = fecha_entrega;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}

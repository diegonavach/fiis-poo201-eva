package com.company;

import java.util.ArrayList;

public class Motorizado extends Persona{
    private double salario;
    private String provincia;
    private String distrito;
    private Motocicleta motocicleta;
    private Empresa empresa;

    public Motorizado(Empresa emp,double salario, String provincia, String distrito,Motocicleta moto,String DNI, String nombre, ArrayList<String> direcciones, int telefono) {
        super(DNI,nombre,direcciones,telefono);
        this.salario = salario;
        this.provincia = provincia;
        this.distrito = distrito;
        this.motocicleta=moto;
        this.empresa=emp;
        empresa.añadirMotorizado(this);
    }

    public void setMotocicleta(Motocicleta motocicleta) {
        this.motocicleta = motocicleta;
    }
}

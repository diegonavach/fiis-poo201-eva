package com.company;

import java.util.ArrayList;

public class Usuario extends Persona{
    private Empresa empresa;
    private String contra;
    private String nombre_u;
    public Usuario(Empresa empresa,String DNI, String nombre, ArrayList<String> direcciones, int telefono,String contra,String nombre_u) {
        super(DNI, nombre, direcciones, telefono);
        this.empresa=empresa;
        empresa.añadirUsuario(this);
        this.contra=contra;
        this.nombre_u=nombre_u;
    }
}
